1. I handle only `Int` type so I can optimize the code in that direction.
2. I expect the tree to have one root and be evenly distributed, ie. n-th level containing n elements.
    This allows me to reverse-build the tree without recursion with limited object creation using immutable Tree data structure.
3. Usage of cats.IO for path finding will create stack-safe computation AST