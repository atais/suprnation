name := "triangle-test"

version := "0.1"

scalaVersion := "2.13.1"

lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.1.1" % Test
lazy val cats = "org.typelevel" %% "cats-core" % "2.1.1"
lazy val catsEffect = "org.typelevel" %% "cats-effect" % "2.1.2"

libraryDependencies ++= Seq(
  scalaTest, cats, catsEffect
)