# Minimum/Maximum Triangle Paths

## Test
```
sbt test
```

## Build
```
sbt assembly
```

## Example run

```
cat << EOF | java -jar "target/scala-2.13/triangle-test-assembly-0.1.jar"
7
6 3
3 8 5
11 2 10 9
EOF
```