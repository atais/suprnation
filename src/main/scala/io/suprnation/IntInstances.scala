package io.suprnation

import cats.Show
import io.suprnation.LineParser.{LPEx, ParsingError}
import io.suprnation.TreePathFinder.Path

import scala.util.{Failure, Success, Try}

object IntInstances extends IntInstances

trait IntInstances {
  implicit val intPathShow: Show[Path[Int]] = new Show[Path[Int]] {
    override def show(t: Path[Int]): String =
      t.nodes.mkString(" + ") + " = " + t.combined
  }

  implicit val intPathOrdering: Ordering[Path[Int]] = new Ordering[Path[Int]] {
    override def compare(x: Path[Int], y: Path[Int]): Int = x.combined.compare(y.combined)
  }

  implicit val intLineParser: LineParser[Int] = new LineParser[Int] {
    override def parse(line: String): Either[LPEx, Array[Int]] =
      Try {
        line.split(' ').map(_.toInt)
      } match {
        case Failure(ex) => Left(ParsingError(line, ex))
        case Success(value) => Right(value)
      }
  }
}
