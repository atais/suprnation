package io.suprnation

import io.suprnation.LineParser._

trait LineParser[A] {

  def parse(line: String): Either[LPEx, Array[A]]

}

object LineParser {

  def apply[A](line: String)(implicit lp: LineParser[A]): Either[LPEx, Array[A]] =
    lp.parse(line)

  sealed trait LPEx

  case class ParsingError(line: String, ex: Throwable) extends LPEx

}
