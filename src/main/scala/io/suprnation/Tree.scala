package io.suprnation

sealed trait Tree[@specialized(Int) +A]

case object Empty extends Tree[Nothing]

case class Node[A](v: A, l: Tree[A], r: Tree[A]) extends Tree[A]

case class Leaf[A](v: A) extends Tree[A]