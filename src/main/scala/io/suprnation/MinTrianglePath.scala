package io.suprnation


import cats.effect.{ExitCode, IO, IOApp}

import scala.collection.mutable
import scala.io.StdIn

// fixme: ugly main
object MinTrianglePath extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = {
    println("Provide lines:")
    val lines = mutable.ListBuffer.empty[String]
    var line = ""
    while ( {
      line = StdIn.readLine();
      line != null
    }) {
      println(line)
      lines += line
    }

    import IntInstances._
    import cats.instances.int._
    import cats.instances.list._
    import cats.syntax.parallel._
    import cats.syntax.show._

    val tree = TreeBuilder.build[Int](lines.toList).right.get

    println("Created tree")
    println(tree)

    val paths = TreePathFinder.allPaths[Int, IO](tree)

    paths.parSequence
      .map { paths =>
        val min = paths.min
        println(min.show)
      }
      .map(_ => ExitCode.Success)
  }
}
