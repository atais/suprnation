package io.suprnation

import cats.effect.Sync
import cats.kernel.Monoid
import cats.syntax.flatMap._

object TreePathFinder {

  def allPaths[A: Monoid, F[_] : Sync](t: Tree[A]): List[F[Path[A]]] =
    path0(t)

  private def path0[A: Monoid, F[_] : Sync](t: Tree[A]): List[F[Path[A]]] =
    t match {
      case Empty =>
        List.empty
      case Leaf(v) =>
        List(Sync[F].delay(Path(v)))
      case Node(v, ln, rn) =>
        val zero = Sync[F].delay(Path(v))
        val l = pathN(ln, zero)
        val r = pathN(rn, zero)
        l ++ r
    }

  private def pathN[A: Monoid, F[_] : Sync](t: Tree[A], soFar: F[Path[A]]): List[F[Path[A]]] =
    t match {
      case Empty =>
        List(soFar)
      case Leaf(v) =>
        val next = soFar.flatMap(p => Sync[F].delay(p.add(v)))
        List(next)
      case Node(v, ln, rn) =>
        val next = soFar.flatMap(p => Sync[F].delay(p.add(v)))
        val l = pathN(ln, next)
        val r = pathN(rn, next)
        l ++ r
    }

  object Path {
    def apply[A: Monoid](el: A): Path[A] = new Path(List(el), el)
  }

  case class Path[@specialized(Int) A: Monoid](nodes: List[A], combined: A) {
    def add(a: A): Path[A] =
      Path(a +: nodes, Monoid.combine(a, combined))
  }

}
