package io.suprnation

import cats.syntax.either._
import io.suprnation.LineParser.LPEx

import scala.util.{Failure, Success, Try}

object TreeBuilder {

  def build[A: LineParser](lines: List[String]): Either[TBEx, Tree[A]] =
    lines.reverse
      .foldLeft(Array.empty[Tree[A]].asRight[TBEx]) { case (tree, line) =>
        for {
          t <- tree
          n <- buildNodes(line, t)
        } yield n
      }
      .flatMap { tree =>
        if (tree.length == 1) {
          Right(tree.head)
        } else {
          Left(MultipleRoots(tree))
        }
      }

  private def buildNodes[A: LineParser](line: String, lowerLevel: Array[Tree[A]]): Either[TBEx, Array[Tree[A]]] =
    LineParser[A](line) match {
      case Left(lpex) =>
        Left(LineException(line, lpex))
      case Right(numbers) if lowerLevel.isEmpty =>
        Right(numbers.map(Leaf(_): Tree[A]))
      case Right(numbers) if numbers.length + 1 != lowerLevel.length =>
        Left(UnevenInputTree(line, lowerLevel))
      case Right(numbers) =>
        Try[Array[Tree[A]]] {
          numbers.zipWithIndex.map { case (v, i) =>
            Node(v, lowerLevel(i), lowerLevel(i + 1))
          }
        } match {
          case Failure(ex) => Left(OtherException(line, lowerLevel, ex))
          case Success(value) => Right(value)
        }
    }

  sealed trait TBEx

  case class LineException(line: String, ex: LPEx) extends TBEx

  case class UnevenInputTree[A](line: String, soFar: Array[Tree[A]]) extends TBEx

  case class MultipleRoots[A](tree: Array[Tree[A]]) extends TBEx

  case class OtherException[A](line: String, soFar: Array[Tree[A]], ex: Throwable) extends TBEx

}