package io.suprnation

import io.suprnation.IntInstances._
import io.suprnation.TreeBuilderTest._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers


class TreeBuilderTest extends AnyFlatSpec with Matchers {

  it should "properly build sample int tree" in {
    val line =
      """7
        |6 3
        |3 8 5
        |11 2 10 9
        |""".stripMargin
    val lines = line.split("\n").toList

    val tree = TreeBuilder.build(lines)

    tree.right.get shouldBe smallExampleTree
  }


}

object TreeBuilderTest {

  val smallExampleTree: Tree[Int] = {
    val (d1, d2, d3, d4) = (Leaf(11), Leaf(2), Leaf(10), Leaf(9))
    val (c1, c2, c3) = (Node(3, d1, d2), Node(8, d2, d3), Node(5, d3, d4))
    val (b1, b2) = (Node(6, c1, c2), Node(3, c2, c3))
    val root = Node(7, b1, b2)
    root
  }

}