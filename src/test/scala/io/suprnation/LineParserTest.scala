package io.suprnation

import io.suprnation.IntInstances._
import io.suprnation.LineParser.ParsingError
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers


class LineParserTest extends AnyFlatSpec with Matchers {

  "IntLineParser" should "properly parse sample line" in {
    val line = "0 1 2 3"
    val els = LineParser[Int](line)

    els.right.get should contain theSameElementsInOrderAs Array(0, 1, 2, 3)
  }

  it should "fail if not int is supplied" in {
    val line = "0 1A 2 3"
    val els = LineParser[Int](line)

    els.left.get.asInstanceOf[ParsingError].line shouldBe line
    els.left.get.asInstanceOf[ParsingError].ex.getClass shouldBe classOf[NumberFormatException]
  }


}
