package io.suprnation

import java.util.concurrent.atomic.AtomicInteger

import cats.effect.IO
import cats.instances.int._
import cats.instances.list._
import cats.syntax.show._
import cats.syntax.traverse._
import io.suprnation.IntInstances._
import io.suprnation.TreePathFinder.Path
import io.suprnation.TreePathFinderTest._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TreePathFinderTest extends AnyFlatSpec with Matchers {

  it should "find all paths in small example tree" in {
    val tree = TreeBuilderTest.smallExampleTree

    val paths: List[IO[Path[Int]]] = TreePathFinder.allPaths[Int, IO](tree)
    val all: Seq[Path[Int]] = paths.sequence.unsafeRunSync()

    val correctPaths = List(
      Path(List(11, 3, 6, 7), 27),
      Path(List(2, 3, 6, 7), 18),
      Path(List(2, 8, 6, 7), 23),
      Path(List(10, 8, 6, 7), 31),
      Path(List(2, 8, 3, 7), 20),
      Path(List(10, 8, 3, 7), 28),
      Path(List(10, 5, 3, 7), 25),
      Path(List(9, 5, 3, 7), 24)
    )

    all should contain theSameElementsAs correctPaths
  }

  val testTreeSize: Int = 20

  it should s"work for input of $testTreeSize rows" in {
    val sg = new StatefulGenerator
    val tree = time("Generating input") {
      treeGenerator(testTreeSize, _ => sg.apply())
    }
    val paths: List[IO[Path[Int]]] = time("Generating AST") {
      TreePathFinder.allPaths[Int, IO](tree)
    }
    val calculated: Seq[Path[Int]] = time("Calculating paths") {
      paths.sequence.unsafeRunSync()
    }

    calculated.size shouldBe math.pow(2, testTreeSize - 1)
    println(calculated.size)
    val min = calculated.min
    println(min.show)
    min.nodes.head shouldBe 1
  }

}

object TreePathFinderTest {

  def time[R](name: String)(block: => R): R = {
    val t0 = System.nanoTime()
    val result = block // call-by-name
    val t1 = System.nanoTime()
    println(name + " took time: " + (t1 - t0) / 1000000 + "ms")
    result
  }

  class StatefulGenerator() {
    private val i = new AtomicInteger(0)

    def apply(): Int = i.incrementAndGet()
  }

  def treeGenerator(depth: Int, valueGen: Int => Int): Tree[Int] = {
    def leavesG(depth: Int): List[Leaf[Int]] = {
      (0 until depth).map(n => Leaf(valueGen(n))).toList
    }

    def generateN(depth: Int, levelLower: List[Tree[Int]]): List[Node[Int]] =
      (0 until depth).map(n => Node(valueGen(n), levelLower(n), levelLower(n + 1))).toList

    val leaves: List[Tree[Int]] = leavesG(depth)
    val tree = (1 until depth).foldLeft(leaves) { case (prev, level) =>
      val nodes = generateN(depth - level, prev)
      nodes
    }
    tree.head
  }

}
